import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-task-one',
  templateUrl: './directives-task-one.component.html',
  styleUrls: ['./directives-task-one.component.css']
})
export class DirectivesTaskOneComponent implements OnInit {

  userNumber: number;

  constructor() {
    this.userNumber = 0;
  }

  ngOnInit() {
  }

}
