import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskOneComponent } from './directives-task-one.component';

describe('DirectivesTaskOneComponent', () => {
  let component: DirectivesTaskOneComponent;
  let fixture: ComponentFixture<DirectivesTaskOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
