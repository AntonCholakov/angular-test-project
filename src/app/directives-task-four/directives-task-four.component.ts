import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-directives-task-four',
  templateUrl: './directives-task-four.component.html',
  styleUrls: ['./directives-task-four.component.css']
})
export class DirectivesTaskFourComponent implements OnInit {

  gender: string;
  other: string;

  constructor() {
    this.gender = 'other';
  }

  ngOnInit() {
  }

  getImageSource(): string {
    switch (this.gender) {
      case 'male':
        return 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png';
      case 'female':
        return 'https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png';
      case 'other':
        if (this.other === 'alien' || this.other === 'ugly') {
          return 'https://www.askideas.com/media/18/Funny-Alien-Cartoon-Image.jpg';
        }

        return 'https://res.cloudinary.com/teepublic/image/private/s--UvSUNgzW--/b_rgb:fffffe,t_Heather%20Preview/c_lpad,f_jpg,h_630,q_90,w_1200/v1494469473/production/designs/1595608_1.jpg';
    }
  }

}
