import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskSixComponent } from './directives-task-six.component';

describe('DirectivesTaskSixComponent', () => {
  let component: DirectivesTaskSixComponent;
  let fixture: ComponentFixture<DirectivesTaskSixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskSixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskSixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
