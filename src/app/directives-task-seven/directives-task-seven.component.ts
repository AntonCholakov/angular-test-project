import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-task-seven',
  templateUrl: './directives-task-seven.component.html',
  styleUrls: ['./directives-task-seven.component.css']
})
export class DirectivesTaskSevenComponent implements OnInit {

  names: string[];

  constructor() {
    this.names = ['John', 'Jack', 'Jenny'];
  }

  ngOnInit() {
  }

  onAdd(name: string): void {
    if (name.length < 2) {
      alert('Name is too short.');
      return;
    }

    console.log(name);
    this.names.push(name);
  }

}
