import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskSevenComponent } from './directives-task-seven.component';

describe('DirectivesTaskSevenComponent', () => {
  let component: DirectivesTaskSevenComponent;
  let fixture: ComponentFixture<DirectivesTaskSevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskSevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskSevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
