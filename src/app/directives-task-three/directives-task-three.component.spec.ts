import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskThreeComponent } from './directives-task-three.component';

describe('DirectivesTaskThreeComponent', () => {
  let component: DirectivesTaskThreeComponent;
  let fixture: ComponentFixture<DirectivesTaskThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
