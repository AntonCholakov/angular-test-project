import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {UserInformationComponent} from './user-information/user-information.component';
import {DirectivesTaskOneComponent} from './directives-task-one/directives-task-one.component';
import {DirectivesTaskTwoComponent} from './directives-task-two/directives-task-two.component';
import {DirectivesTaskThreeComponent} from './directives-task-three/directives-task-three.component';
import {DirectivesTaskFourComponent} from './directives-task-four/directives-task-four.component';
import {DirectivesTaskFiveComponent} from './directives-task-five/directives-task-five.component';
import {DirectivesTaskSixComponent} from './directives-task-six/directives-task-six.component';
import {DirectivesTaskSevenComponent} from './directives-task-seven/directives-task-seven.component';
import {HomeComponent} from './home/home.component';
import {AppRoutingModule} from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserInformationComponent,
    DirectivesTaskOneComponent,
    DirectivesTaskTwoComponent,
    DirectivesTaskThreeComponent,
    DirectivesTaskFourComponent,
    DirectivesTaskFiveComponent,
    DirectivesTaskSixComponent,
    DirectivesTaskSevenComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
