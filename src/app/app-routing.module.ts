import {NgModule} from '@angular/core';
import {HomeComponent} from './home/home.component';
import {RouterModule, Routes} from '@angular/router';
import {DirectivesTaskOneComponent} from './directives-task-one/directives-task-one.component';
import {DirectivesTaskTwoComponent} from './directives-task-two/directives-task-two.component';
import {DirectivesTaskThreeComponent} from './directives-task-three/directives-task-three.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'task-one',
    component: DirectivesTaskOneComponent
  },
  {
    path: 'task-two',
    component: DirectivesTaskTwoComponent
  },
  {
    path: 'task-three',
    component: DirectivesTaskThreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
