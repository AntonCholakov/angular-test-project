import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskFourComponent } from './directives-task-four.component';

describe('DirectivesTaskFourComponent', () => {
  let component: DirectivesTaskFourComponent;
  let fixture: ComponentFixture<DirectivesTaskFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
