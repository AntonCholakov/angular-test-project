import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskFiveComponent } from './directives-task-five.component';

describe('DirectivesTaskFiveComponent', () => {
  let component: DirectivesTaskFiveComponent;
  let fixture: ComponentFixture<DirectivesTaskFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
