import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-task-five',
  templateUrl: './directives-task-five.component.html',
  styleUrls: ['./directives-task-five.component.css']
})
export class DirectivesTaskFiveComponent implements OnInit {

  selectedFontColor: string;
  selectedBackgroundColor: string;

  constructor() {
    this.selectedFontColor = 'black';
    this.selectedBackgroundColor = 'transparent';
  }

  ngOnInit() {
  }

}
