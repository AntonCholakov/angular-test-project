import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-directives-task-six',
  templateUrl: './directives-task-six.component.html',
  styleUrls: ['./directives-task-six.component.css']
})
export class DirectivesTaskSixComponent implements OnInit {

  userInput: string;
  selectedAlertType: string;

  constructor() {
    this.userInput = '';
    this.selectedAlertType = 'alert-dark';
  }

  ngOnInit() {
  }

}
