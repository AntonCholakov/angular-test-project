import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivesTaskTwoComponent } from './directives-task-two.component';

describe('DirectivesTaskTwoComponent', () => {
  let component: DirectivesTaskTwoComponent;
  let fixture: ComponentFixture<DirectivesTaskTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectivesTaskTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivesTaskTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
