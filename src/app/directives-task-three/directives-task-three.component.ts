import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-task-three',
  templateUrl: './directives-task-three.component.html',
  styleUrls: ['./directives-task-three.component.css']
})
export class DirectivesTaskThreeComponent implements OnInit {

  username: string;
  password: string;

  constructor() { }

  ngOnInit() {
  }

}
