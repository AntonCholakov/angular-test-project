import {Component} from '@angular/core';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.css'],
})
export class UserInformationComponent {

  name: string;
  age: number;
  address: string;
  isMale: boolean;
  profilePicture: string;

  email: string;
  password: string;

  today: Date;

  constructor() {
    this.name = 'John';
    this.age = 22;
    this.address = 'Cat Street 22';
    this.isMale = true;
    this.profilePicture = 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png';

    this.email = 'john@doe.com';

    this.today = new Date();
  }

  onProfilePictureChange(): void {
    if (this.profilePicture === 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png') {
      this.profilePicture = 'https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png';
    } else {
      this.profilePicture = 'https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png';
    }
  }

  onIncreaseAge(): void {
    this.age++;
  }

  onNameChange(name: string): void {
    this.name = name;
  }

  onSubmit(): void {
    console.log('Email: ', this.email);
    console.log('Password: ', this.password);
  }

  onIsMaleChange(event): void {
    console.log(event);
  }

  getName(): string {
    return this.name.toUpperCase();
  }

}
