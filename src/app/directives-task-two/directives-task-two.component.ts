import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-task-two',
  templateUrl: './directives-task-two.component.html',
  styleUrls: ['./directives-task-two.component.css']
})
export class DirectivesTaskTwoComponent implements OnInit {

  isContentDisplayed: boolean;

  constructor() { }

  ngOnInit() {
  }

}
